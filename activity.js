// Single room

db.singleRoom.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the baisc necessities",
	rooms_available: 10,
	isAvailable: "false"
});
// Multiple rooms

db.MultipleRooms.insertMany([{
	name: "double",
	accomodates: 3,
	price: 2000,
	description: "A room fit for a small family going on a vacation",
	rooms_available: 5,
	isAvailable: "false"
},
{
	name: "queen",
	accomodates: 4,
	price: 4000,
	description: "A room with a queen sized bed perfecr for a simple getaway",
	rooms_available: 15,
	isAvailable: "false"
}
])

// find method

db.MultipleRooms.find({name: "double"})

// updateOne method

db.MultipleRooms.updateOne({rooms_available: 15},{$set: {rooms_available: 0}})

// Delete many
db.MultipleRooms.deleteMany({rooms_available: 0})